
CFLAGS = -Wall -fPIC -I /usr/include/apr-1.0 -I /usr/include/httpd
LDFLAGS = -shared
LDLIBS = -lmysqlclient -lcrypto

MODULE=mod_authnz_mysql

default: .libs/$(MODULE).so test

.libs/%.so: %.c
	@echo " CC  $^"
	@apxs -c $(LDLIBS) -o $*.so $^

.PHONY: install
install: .libs/$(MODULE).so
	@echo "INST $^"
	@apxs -i -n $(MODULE) $<

clean:
	rm -f *.o *.la *.lo *.sla *.slo -r .libs

test: CFLAGS=-Wall -O0 -g3
test: LDFLAGS =
test: LDLIBS = -lcrypto
