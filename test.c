#include <errno.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#include <unistd.h>
#include <fcntl.h>

#include <openssl/hmac.h>
#include <openssl/evp.h>

int main(int argc, char *argv[])
{
	uint8_t out[128];
	unsigned int outlen;
	int j;
	void *res;
	char in[1024];
	char *intok;
	int inlen;

	if (argc < 3) {
		fprintf(stderr, "usage: %s DIGEST SALT\n", argv[0]);
		exit(1);
	}

	OpenSSL_add_all_digests();

	const EVP_MD *evpmd;
	char *key;

	evpmd = EVP_get_digestbyname(argv[1]);
	if (!evpmd) {
		fprintf(stderr, "unknown digest '%s'", argv[1]);
		return 1;
	}
	key = argv[2];

	inlen = read(STDIN_FILENO, in, sizeof(in)-1);
	if (in < 0) {
		fprintf(stderr, "read stdin: %s", strerror(errno));
		exit(1);
	}
	in[inlen] = 0;
	intok = strtok(in, "\r\n") ?: "";
	inlen = strlen(intok ?: "");
	if (!inlen)
		fprintf(stderr, "empty token on stdin");

	outlen = sizeof(out);
	res = HMAC(evpmd,
			(void *)key, strlen(key),
			(void *)intok, inlen,
			out, &outlen);
	if (!res)
		return 1;
	for (j = 0; j < outlen; ++j)
		printf("%02x", out[j]);
	printf("\n");
	return 0;
}
