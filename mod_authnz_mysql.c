#include <apr_strings.h>

#include <ap_config.h>
#include <ap_provider.h>
#include <httpd.h>
#include <http_config.h>
#include <http_core.h>
#include <http_log.h>
#include <http_protocol.h>
#include <http_request.h>

#include <mod_auth.h>

#include <openssl/hmac.h>
#include <openssl/evp.h>
#include <mysql/mysql.h>

#define MAXLEN 1024

struct config {
	char *host;
	char *user;
	char *pw;
	char *db;

	/* authn */
	char *query;
	int nevps;
	const EVP_MD **evps;
	char *salt;

	/* authz */
	struct myauthz {
		const char *name;
		const char *query;
	} *authz;
	int nauthz, sauthz;

	MYSQL *link;
};

static int digests_loaded;

static const char *config_types(cmd_parms *cmd, void *vcfg, int argc, char *const argv[])
{
	int j;
	struct config *cfg = vcfg;

	if (!digests_loaded) {
		digests_loaded = 1;
		OpenSSL_add_all_digests();
	}

	cfg->evps = apr_palloc(cmd->pool, sizeof(cfg->evps[0])*argc);
	cfg->nevps = 0;

	for (j = 0; j < argc; ++j) {
		cfg->evps[cfg->nevps] = EVP_get_digestbyname(argv[j]);
		if (!cfg->evps[cfg->nevps])
			ap_log_error(APLOG_MARK, APLOG_WARNING, 0, 0, "unknown digest '%s'", argv[j]);
		else
			++cfg->nevps;
	}
	return cfg->nevps ? NULL : "no password types";
}

static const char *config_authz(cmd_parms *cmd, void *vcfg, const char *p1, const char *p2)
{
	int j;
	struct config *cfg = vcfg;

	if (cfg->nauthz +1 >= cfg->sauthz) {
		struct myauthz *newauthz;

		cfg->sauthz += 16;
		newauthz = apr_palloc(cmd->pool, sizeof(*newauthz)*cfg->sauthz);
		memcpy(newauthz, cfg->authz, sizeof(*newauthz)*cfg->nauthz);
		cfg->authz = newauthz;
	}
	cfg->authz[cfg->nauthz].name = p1;
	cfg->authz[cfg->nauthz].query = p2;
	++cfg->nauthz;
	return NULL;
}

static const command_rec params [] = {
	AP_INIT_TAKE1("AuthMySQLHost"		, ap_set_string_slot, (void *)APR_OFFSETOF(struct config, host), OR_AUTHCFG, "database host"),
	AP_INIT_TAKE1("AuthMySQLUsername"	, ap_set_string_slot, (void *)APR_OFFSETOF(struct config, user), OR_AUTHCFG, "database user" ),
	AP_INIT_TAKE1("AuthMySQLPassword"	, ap_set_string_slot, (void *)APR_OFFSETOF(struct config, pw), OR_AUTHCFG, "database pwd" ),
	AP_INIT_TAKE1("AuthMySQLDatabase"	, ap_set_string_slot, (void *)APR_OFFSETOF(struct config, db), OR_AUTHCFG, "database name" ),
	AP_INIT_TAKE1("AuthMySQLNQuery"		, ap_set_string_slot, (void *)APR_OFFSETOF(struct config, query), OR_AUTHCFG, "database query for authn, return password(s)" ),
	AP_INIT_TAKE_ARGV("AuthMySQLNType"	, config_types, 0, OR_AUTHCFG, "password hmac digest type(s)" ),
	AP_INIT_TAKE1("AuthMySQLNSalt"		, ap_set_string_slot, (void *)APR_OFFSETOF(struct config, salt), OR_AUTHCFG, "password hmac salt" ),
	AP_INIT_TAKE2("AuthMySQLZQuery"		, config_authz, 0, OR_AUTHCFG, "name + database query for authz, return rows or not" ),
	{NULL}
};

module AP_MODULE_DECLARE_DATA authnz_mysql_module;

static void *create_dir_config(apr_pool_t *p, char *d)
{
	struct config *cfg = apr_palloc(p, sizeof(*cfg));
	memset(cfg, 0, sizeof(*cfg));
	return cfg;
}

static int cleanup_config(void *data)
{
	struct config *cfg = data;

	if (cfg->link) {
		mysql_close(cfg->link);
		cfg->link = NULL;
		ap_log_error(APLOG_MARK, APLOG_NOTICE, 0, 0, "mysql %s down", cfg->host);
	}
	return 0;
}

static int open_db(struct config *cfg)
{
	if (cfg->link)
		return 0;

	MYSQL *h;

	h = mysql_init(NULL);
	if (!h) {
		ap_log_error(APLOG_MARK, APLOG_ERR, 0, 0, "mysql_init failed");
		return HTTP_SERVICE_UNAVAILABLE;
	}
	if (!mysql_real_connect(h, cfg->host ?: "localhost", cfg->user, cfg->pw, cfg->db, 3306, 0, CLIENT_COMPRESS)) {
		ap_log_error(APLOG_MARK, APLOG_ERR, 0, 0, "mysql_real_connect %s failed: %s", cfg->host ?: "localhost", mysql_error(h));
		mysql_close(h);
		return HTTP_SERVICE_UNAVAILABLE;
	}
	cfg->link = h;
	ap_log_error(APLOG_MARK, APLOG_NOTICE, 0, 0, "mysql %s up", cfg->host);
	return 0;
}

static char *db_escape(request_rec *req, MYSQL *mysql, const char *str)
{
	char *a;
	int len;

	len = strlen(str)*2 +1;
	a = apr_palloc(req->pool, len);
	if (!a)
		ap_log_error(APLOG_MARK, APLOG_ERR, 0, 0, "malloc %i failed: %s", len, strerror(errno));
	mysql_real_escape_string(mysql, a, str, strlen(str));
	return a;
}

static char *auth_mysql_fmt_query(request_rec *req, MYSQL *mysql, const char *cquery,
		const char *user)
{
	int pos, len, curlen, vlen;
	char *str, *end, *key, *newq;
	char *query;
	const char *value;

	/* preset current length */
	curlen = strlen(cquery);
	/* align block to 1k */
	len = (curlen+1+1023)&~1023;
	query = apr_palloc(req->pool, len);
	strcpy(query, cquery);

	for (pos = 0;;) {
		str = strstr(query+pos, "%{");
		if (!str)
			/* no more replacements */
			break;
		end = strchr(str+2, '}');
		if (!end)
			break;
		/* prepare key */
		*end = 0;
		key = str+2;
		if (!strcmp(key, "USER"))
			value = user;
		else
			/* lookup in env */
			value = apr_table_get(req->subprocess_env, key);
		/* restore \}*/
		*end = '}';
		if (!value) {
			/* nothing to replace */
			pos = end+1-query;
			continue;
		}
		value = db_escape(req, mysql, value);
		vlen = strlen(value);
		if (curlen+vlen-(strlen(key)+3)+1 > len) {
			/* realloc */
			len = curlen+vlen-(strlen(key)+3)+1;
			/* align */
			len = (len+1023)%~1023;
			newq = apr_palloc(req->pool, len);
			strcpy(newq, query);
			/* reposition str & end */
			str = newq+(str-query);
			end = newq+(end-query);
			/* assign */
			query = newq;
		}
		/* replace inline */
		memmove(str+vlen, end+1, strlen(end+1)+1);
		memcpy(str, value, vlen);
	}
	ap_log_error(APLOG_MARK, APLOG_DEBUG, 0, 0, "query: %s", query);
	return query;
}

static void bin2hex (const unsigned char *bin, int len, char *dst)
{
	const unsigned char * end = &bin[len];
	static const char hex [] = "0123456789abcdef";

	for (; bin < end; bin += 1, dst += 2) {
		dst[0] = hex[bin[0] >> 4];
		dst[1] = hex[bin[0] & 0x0f];
	}
	*dst = 0;
}

static authn_status auth_mysql_authenticate(request_rec *req, const char *usr, const char *pwd)
{
	authn_status result = AUTH_GENERAL_ERROR;
	char *q;
	struct config *cfg = ap_get_module_config(req->per_dir_config, &authnz_mysql_module);

	if (!cfg)
		return AUTH_USER_NOT_FOUND;
	if (!cfg->query)
		return AUTH_USER_NOT_FOUND;

	if (0 != open_db(cfg))
		return 0;

	q = auth_mysql_fmt_query(req, cfg->link, cfg->query, usr);

	if (mysql_real_query(cfg->link, q, strlen(q))) {
		ap_log_error(APLOG_MARK, APLOG_ERR, 0, 0, "query %s failed: %s", q, mysql_error(cfg->link));
		goto failed;
	}

	/* process result */
	MYSQL_RES * mres;
	mres = mysql_store_result(cfg->link);
	if (!mres) {
		if (mysql_field_count(cfg->link))
			ap_log_error(APLOG_MARK, APLOG_ERR, 0, 0, "query %s failed: %s", q, mysql_error(cfg->link));
		else
			ap_log_error(APLOG_MARK, APLOG_ERR, 0, 0, "query %s with no result", q);
		goto failed;
	}

	unsigned long nrows = mysql_num_rows(mres);
	unsigned long ncols = mysql_num_fields(mres);
	if (!nrows) {
		result = AUTH_USER_NOT_FOUND;
		goto failed_result;
	}
	if (nrows > 1) {
		result = AUTH_DENIED;
		ap_log_error(APLOG_MARK, APLOG_WARNING, 0, 0, "user %s has multiple matches", usr);
		goto failed_result;
	}
	if (!ncols) {
		result = AUTH_USER_NOT_FOUND;
		ap_log_error(APLOG_MARK, APLOG_WARNING, 0, 0, "query returned no columns");
		goto failed_result;
	}

	MYSQL_ROW mrow;
	unsigned long * sizes;
        mrow = mysql_fetch_row(mres);
	sizes = mysql_fetch_lengths(mres);

	unsigned char *binhash = apr_palloc(req->pool, MAXLEN);
	char *strhash = apr_palloc(req->pool, MAXLEN*2+1);

	/* iterate types, and try every hash type against every column
	 * if the length of the column matches the expected hash type length
	 */
	int t, c;
	unsigned int hashlen;
	for (t = 0; t < cfg->nevps; ++t) {
		memset(binhash, 0, MAXLEN);
		HMAC(cfg->evps[t], cfg->salt ?: "", strlen(cfg->salt ?: ""),
				(const void *)pwd, strlen(pwd), binhash, &hashlen);
		bin2hex(binhash, hashlen, strhash);

		for (c = 0; c < ncols; ++c) {
			if (sizes[c] == hashlen*2 && mrow[c] &&
					!strncmp(mrow[c], strhash, sizes[c])) {
				result = AUTH_GRANTED;
				goto done;
			}
		}
	}
	result = AUTH_DENIED;

done:
failed_result:
	mysql_free_result(mres);
failed:
	return result;
}

static const authn_provider this_provider = {
	&auth_mysql_authenticate,
};

/* authz */
static authz_status auth_mysql_authorize(request_rec *req,
                                             const char *require_args,
                                             const void *parsed_require_args)
{
	struct config *cfg = ap_get_module_config(req->per_dir_config, &authnz_mysql_module);
	int pos, len, curlen, vlen;
	char *q, *str, *end, *key, *value;
	char *requiretok;
	authz_status result = AUTHZ_GENERAL_ERROR;

	if (!req->user)
		return AUTHZ_DENIED_NO_USER;

	if (!cfg->nauthz)
		return AUTHZ_NEUTRAL;

	if (0 != open_db(cfg))
		return AUTHZ_GENERAL_ERROR;

	for (requiretok = strtok(apr_pstrdup(req->pool, require_args), " \t"); requiretok; requiretok = strtok(NULL, " \t")) {
		int j;

		/* lookup query */
		for (j = 0; j < cfg->nauthz; ++j) {
			if (!strcmp(requiretok, cfg->authz[j].name))
				break;
		}
		if (j >= cfg->nauthz) {
			ap_log_error(APLOG_MARK, APLOG_WARNING, 0, 0, "unknown item '%s' in require mysql", requiretok);
			continue;
		}

		q = auth_mysql_fmt_query(req, cfg->link, cfg->authz[j].query, req->user);

		if (mysql_real_query(cfg->link, q, strlen(q))) {
			ap_log_error(APLOG_MARK, APLOG_ERR, 0, 0, "query %s failed: %s", q, mysql_error(cfg->link));
			continue;
		}

		/* process result */
		MYSQL_RES * mres;
		mres = mysql_store_result(cfg->link);
		if (!mres) {
			if (mysql_field_count(cfg->link))
				ap_log_error(APLOG_MARK, APLOG_ERR, 0, 0, "query %s failed: %s", q, mysql_error(cfg->link));
			else {
				ap_log_error(APLOG_MARK, APLOG_ERR, 0, 0, "query %s with no result", q);
			}
			continue;
		}

		unsigned long nrows = mysql_num_rows(mres);
		unsigned long ncols = mysql_num_fields(mres);
		mysql_free_result(mres);
		if (!ncols) {
			ap_log_error(APLOG_MARK, APLOG_WARNING, 0, 0, "query returned no columns");
			continue;
		}
		if (nrows)
			return AUTHZ_GRANTED;
	}
	return AUTHZ_DENIED;
}

static const char *authz_mysql_parse_config(cmd_parms *cmd, const char *require_line,
                                          const void **parsed_require_line)
{
    return NULL;
}

static const authz_provider authz_mysql_provider =
{
    &auth_mysql_authorize,
    &authz_mysql_parse_config,
};

static void register_hooks(apr_pool_t *p)
{
	ap_register_provider(p, AUTHN_PROVIDER_GROUP, "mysql", "0",
			&this_provider);
	ap_register_auth_provider(p, AUTHZ_PROVIDER_GROUP, "mysql",
			AUTHZ_PROVIDER_VERSION,
			&authz_mysql_provider, AP_AUTH_INTERNAL_PER_CONF);
}

AP_DECLARE_MODULE(authnz_mysql) = {
	STANDARD20_MODULE_STUFF,
	create_dir_config,		/* dir config creater */
	NULL,				/* dir merger --- default is to override */
	NULL,				/* server config */
	NULL,				/* merge server config */
	params,				/* command apr_table_t */
	register_hooks			/* register hooks */
};
