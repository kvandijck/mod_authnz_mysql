# mod_authnz_mysql

Authenticate (authn) and Authorize (authz) via MySQL.

## configuration

```
	<directory ...>
		AuthMySQLHost	mysql.host.name
		AuthMySQLUsername	'mysqluser'
		AuthMySQLPassword	'secret'
		AuthMySQLDatabase	dbname

		AuthMySQLNQuery	"select password from usertable where username = '%{USER}'"
		AuthMySQLNType	sha1 sha256 sha512
		AuthMySQLNSalt	'mysecretsalt'

		AuthMySQLZQuery	mygrp "select gid from usertable as u left join usergrouptable as ug on ug.user = u.id left join grouptable as g on g.id = ug.group where user = '%{USER}' and group = 'specialgroup'"
		require mysql mygrp
	</directory>
```

### `AuthMySQLNQuery` `AuthMySQLNType`

This query should return **exactly 1** row, and each column contains a hashed password.

The authenticator loops over all columns, and all hash types to find a match with the supplied password

### `AuthMySQLNSalt`

The `salt` or `key` to use in OpenSSL's `HMAC()` call

### `AuthMySQLZQuery`

A query for authorization.
Access to a resource is granted when this query returns 1 or more rows.
Access is denied (or delegated) if no rows return.

Use of mod_authnz_mysql authorization is activated by
```
	require mysql
```

### Query formatter

Before sending the query to MySQL, it is run through a formatter.
All `%{XXX}` instances are replaces by the environment variables, after escape.

`%{USER}` is either the user to authenticate (authn) or the authenticated user to authorize (authz).

`XXX` can be anything, also look at <https://httpd.apache.org/docs/2.4/mod/mod_setenvif.html>

consider this example. Access to files /path/to/file-GID.ext is restricted to members of group with id GID.

```
	<directory xxx>
		SetEnvIf Request_URI "-([0-9]*)\." GID=$1
		AuthMySQLZQuery	mygid "select ug.group from usertable as u left join usergrouptable as ug on ug.user = u.id where user = '%{USER}' and ug.group = %{GID}"
		require mysql mygid
	</directory>
```

